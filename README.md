<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [skc-packer-base-ubunut](#skc-packer-base-ubunut)
    - [this will buid a base ubuntu system and save image to aws](#this-will-buid-a-base-ubuntu-system-and-save-image-to-aws)
        - [base image in AWS](#base-image-in-aws)
    - [setup to use the salt-masterless provisioner](#setup-to-use-the-salt-masterless-provisioner)
        - [required plugin](#required-plugin)
    - [example .env file](#example-env-file)

<!-- markdown-toc end -->




# skc-packer-base-ubunut


## this will buid a base ubuntu system and save image to aws

### base image in AWS
- [x] Canonical, Ubuntu Server Pro, 22.04 LTS, amd64 jammy image build on 2023-12-07

```config
sconklin@ip-172-31-18-48:~$ cat /etc/os-release 
PRETTY_NAME="Ubuntu 22.04.3 LTS"
NAME="Ubuntu"
VERSION_ID="22.04"
VERSION="22.04.3 LTS (Jammy Jellyfish)"
VERSION_CODENAME=jammy
ID=ubuntu
ID_LIKE=debian
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
UBUNTU_CODENAME=jammy

```


## setup to use the salt-masterless provisioner


### required plugin
https://github.com/hashicorp/packer-plugin-salt


```
packer {
  required_plugins {
    salt = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/salt"
    }
  }
}

```

## example .env file

```bash
AWS_ACCESS_KEY_ID=your-access-key-id
AWS_SECRET_ACCESS_KEY=your-secret-access-key
AWS_DEFAULT_REGION=us-east-2
AWS_DEFAULT_OUTPUT=table
```

