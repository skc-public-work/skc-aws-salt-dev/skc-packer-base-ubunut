




skc_ami_name     = "skc_base_ubuntu-22-04_linux"
skc_instance_type    = "t3.micro"
skc_source_ami   = "ami-0bddc40b31973ff95"
skc_ssh_username = "ubuntu"
skc_region       = "us-east-2"
skc_vpc_id  = "vpc-00a56302e08a73a08"
skc_subnet_id    = "subnet-01ea4ae8012f6201a"
skc_tag_one      = "skc-ubuntu-22-04-base-0.0.0"
