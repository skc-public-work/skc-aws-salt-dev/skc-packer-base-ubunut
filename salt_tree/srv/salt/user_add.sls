

# user_add
sconklin:
  user.present:
    - fullname: Stephen Conklin
    - shell: /bin/bash
    - home: /home/sconklin
    # - uid: 4000
    # - gid: 4000
    # - groups:
    #   - wheel
    #   - storage
    #   - games

# add the .ssh dir
/home/sconklin/.ssh:
  file.directory:
    - user: sconklin
    - group: sconklin
    - mode: 700
    - makedirs: True
    
# add the authorized keys file so i can use key login
/home/sconklin/.ssh/authorized_keys:
   file.managed:
    - source:
      - salt://authorized_keys
    - user: sconklin
    - group: sconklin
    - mode: '0600'


# give sconklin sudo
/etc/sudoers.d/90-cloud-sconklin-users:
  file.managed:
    - source:
      - salt://90-cloud-sconklin-users
    - user: root
    - group: root
    - mode: '0440'



