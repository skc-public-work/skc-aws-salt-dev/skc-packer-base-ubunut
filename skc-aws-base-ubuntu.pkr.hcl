
variable "aws_access_key" {
  type    = string
  default = env("AWS_ACCESS_KEY_ID")
}

variable "aws_secret_key" {
  type    = string
  default = env("AWS_SECRET_ACCESS_KEY")
}

variable "skc_ami_name" {
  type    = string
}

variable "skc_instance_type" {
  type    = string
}

variable "skc_source_ami" {
  type    = string
}

variable "skc_ssh_username" {
  type    = string
}

variable "skc_region" {
  type    = string
}

variable "skc_vpc_id" {
  type    = string
}

variable "skc_subnet_id" {
  type    = string
}

variable "skc_tag_one"{
  type    = string
}


#==========================================================================


source "amazon-ebs" "skc_base_ubuntu_server" {
  secret_key    = "${var.aws_secret_key}"
  access_key    = "${var.aws_access_key}"
  ami_name      = "${var.skc_ami_name}"
  instance_type = "${var.skc_instance_type}"
  source_ami    = "${var.skc_source_ami}"
  ssh_username  = "${var.skc_ssh_username}"
  region        = "${var.skc_region}"
  vpc_id        = "${var.skc_vpc_id}" 
  subnet_id     = "${var.skc_subnet_id}"
  associate_public_ip_address = true
  tags = {
    Name = "${var.skc_tag_one}"
  }
}


build {
  sources = [
    "source.amazon-ebs.skc_base_ubuntu_server"
    
  ]

  provisioner "shell" {
     inline = ["cat /etc/os-release"]
   }

  provisioner "salt-masterless"{
    local_state_tree = "./salt_tree/srv/salt"
  }

   provisioner "shell" {
    inline = ["sudo rm -rf /srv/salt"]
    execute_command = "sudo sh -c '{{ .Vars }} {{ .Path }}'"
  }


}
